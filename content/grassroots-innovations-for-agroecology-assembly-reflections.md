# ‘Grassroots Innovations for Agroecology Assembly’ Reflections

October 2023 Event

# Context

In October 2023, three OpenTEAM staff members had the unique opportunity to participate in and facilitate the first convening of the ‘Grassroots Innovations for Agroecology Assembly in beautiful Gallesse, Italy. Over three days, our team engaged with representatives from organizations around the world to explore the intersections of agroecology, community connections, and technology. The event, designed to foster collaboration, emphasized grounding our work in the local community, brought forth innovative facilitation techniques, and encouraged collective governance and participant-driven sessions. The group covered topics including the meaning of innovation and what it means to be an innovator in agroecology, how to convene people around innovation and engage in community, and common challenges. This document summarizes takeaways from this inspiring event that have the potential to benefit OpenTEAM internally and within the context of the wider community.

# Innovation

- Stress the importance of adaptation and innovation in governance.

- Emphasize the incentive and demonstrated need for creating something new. Before creating something, consider if it already exists and if something new adds value to the world

- Emphasize scaling out rather than scaling up. Focus on "GLOCAL" - global and local perspectives

# Grounding of Work and Community Connections

- Focus on tying in Agroecology to balance technology and center on common values. Participants brought up agroecology in nearly all conversations

- Emphasize farmer connections and appreciation.

- Bring in local community members, connecting with the place and people. On our final night in Gallese, locals were invited to join a book discussion (with translation).

#  Venue and Event Organization Takeaways

- Choose a single venue with shared seating, a buffet, and long tables. This setting made it simple for participants to engage with one another outside of structured settings. Having meals together built relationships.

- Consider different spaces at the venue for variety. We rotated between the main room we planned to use for the gathering as well as outdoor spaces around the venue. There was a notable shift in energy when we were able to change our setting. There was an improvement of conversations through engagement with the land, including walking exercises and the use of different venue spaces.

- Reflect on the positive aspects of the Italy venue, including the hospitality. We loved our hosts! The kind and welcoming atmosphere and farm setting were a great bonus.

# Pre-Event and Goal-Setting

- Conduct pre-event online sessions for participants to get acquainted.

- Set clear goals and expectations for the convening.

- Establish Signal groups for effective communication.

- Focus on aligning intentions with collaborative processes. Come to the conference with ideas for how to achieve goals (but be open to shifting).

Facilitation

- Incorporate creative exercises, consensus development mechanisms, and other innovative techniques.

- Highlight the potential for collective governance and the benefits of all participants being ‘co-leaders’ of the event.

- Acknowledge the importance of participant ideas and ad-hoc sessions in shaping the event.  Putting energy into listening, supporting ideas, and creating space for flexibility towards new ideas.

- Explore assigning different roles and building in time for planning successful sessions

- Explore assigning different roles and building in time for planning successful sessions

- Reference-specific techniques were used and discovered during this event [in this document](https://docs.google.com/document/d/1yVNw6Av5Y5zUytmm42h2_rCS5hn_AUf5WHn_KqEz0Gs/edit?usp=sharing). 

#  Global Network and Alignment

- Establishing global networks linked through agroecology allows further collaboration global support and knowledge-sharing

- Alignment events like this with FAO, civil society, and philanthropic gatherings brought more energy to the event and opportunities for individuals. We were able to attend the one day of the CSIPM Forum (21 - 22 October) and the assembly was able to stay for the  CFS51 Plenary Session (23 - 27 October) if desired.

- There is a great opportunity to draw inspiration from organizations like the Honeybee Network. *The Honeybee Network serves as a model for the integration of local community structures with larger national institutions. Notably, it employs a unique innovation collection system featuring a prize structure that celebrates excellence in documentation and the discovery of innovations. The network fosters crucial professional development pathways and establishes a structured matrix for evaluating and valuing innovation. Operated by students at universities, it maintains a database of innovations and problem statements, with student projects based on this repository. Recognition for outstanding solutions and documentation emphasizes the public benefits provided by students and celebrates the work of innovators. Students receive credit for their public support, with special training emphasizing humility and the delivery of value to land stewards to build trust and encourage knowledge sharing. The organization receives institutional support for attribution, license creation, defense, and overall support, initially backed by government seed funding and subsequently expanded through contributions from private funders.*

# Reflections on Specific OpenTEAM Workstreams 

## The FAIR Tech Ecosystem Registry

- Define the role of developing a proof of concept for OpenTEAM's dev network.

- Document outcomes and processes for a potential roadmap.

- Provide documentation, process recommendations, and resources for a trusted registry.

## Data Use Work

- Explore new opportunities in translation and global adaptation.

- Consider Innovation Network Assembly for translation help, and build a network of translators for data use agreements.

## Community Stewardship of Documents (applicable across OpenTEAM)

- Learn from CAPE's wiki for engagement and solutions.

- Introduce the concept of data trust.

- Highlight the importance of attribution and sharing adapted resources.

## Governance and Funding

- Highlight the need for a common language and sustained support for governance.

- Explore the concept of "trusts" as a legal structure.

- Consider academia as a source of documentation and dissemination.

- Discuss funding for translation, prototypes, and shared funding pool

# Additional Resources 

[Grassroots Technological Innovations for Agroecology](https://openteamag.gitlab.io/grassroots-innovation-networks/) (website)

White paper [Technological autonomy not autonomous technology](https://gitlab.com/OpenTEAMAg/grassroots-innovation-networks/-/raw/master/docs/assets/Technological_autonomy.pdf)

[CFS 2023/51/5 - CFS Policy Recommendations on Strengthening Collection and Use of Food Security and Nutrition (FSN) data and rel](https://www.fao.org/3/nn152en/nn152en.pdf) 

[Data Working Group - CSIPM](https://www.csm4cfs.org/policy-working-groups/data/) 
