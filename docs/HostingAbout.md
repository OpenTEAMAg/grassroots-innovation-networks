<h1> About the Data Hosting and Storage Agreement </h1>
<p> Data Hosting and Storage Agreements document is a boilerplate policy for organizations who develop or maintain technology platforms that store or host private personal or agricultural data. </p>

<p> This document defines the standard for hosting, storing, and securing producer data as a means of upholding the data privacy and security standard expected by the OpenTEAM community. We expect all partners who host or store agricultural data to adopt this agreement, and to provide feedback within the versioning process. </p>

Relation to other documents in OpenTEAM’s Suite of Ag Data Use Agreements:
References definitions in the Ag Data Use Glossary
Maintains many of the rights defined in the Agriculturalists’ Bill of Data Rights, such as the right to privacy and security, erasure, access and use, portability, etc. 
Note, the right to maintain ownership and control of data, as well as the right to benefit from data, are upheld within Conditional Data Use Agreements  

<p> Relation to other documents in OpenTEAM’s Ag Data Use Suite of Documents: </p>

<ol>
  <li>References definitions in the Ag Data Use Glossary</li>
  <li>Maintains many of the rights defined in the Agriculturalists’ Bill of Data Rights, such as the right to privacy and security, erasure, access and use, portability, etc.  </li>
  <li><i>Note, the right to maintain ownership and control of data, as well as the right to benefit from data, are upheld within Conditional Data Use Agreements’</i> </li>
</ol>

<h1> Versioning </h1>

<i> Version 1 Document (Released July 2023) </i>
<p>We are currently accepting feedback for Version 1.0 of the Oath of Care Starting in 2024, there will be an annual review and versioning process, with the first iterative process taking place quarter one 2024.</p>

<p>Feedback is encouraged at any time. The versioning cycle will be announced with ample time for review in OpenTEAM Working Groups (Hub & Networks, Field Methods, Tech) and in our newsletter.</p>

<h1> Provide Feedback </h1>
You may provide feedback or insights on the Data Hosting and Storage Agreemen in the following ways:
<ol>
  <li>Visit and contribute to the discussion on Hylo [link] </li>
  <li>Provide comments on the linked and embedded Google doc below (to submit a comment anonymously, log out of your Google account or open the link in a private (incognito) window</li>
  <li>Contact us directly [link to contact page]</li>
</ol>

<h2> <a href="https://docs.google.com/document/d/1CE0w7Pty9Vw6DGu2k9AYZlBDCehiIRRRwY8jCNduDEU/edit?usp=sharing"> Provide Comments to the Data Hosting and Storage Agreement</a> </h2>
<iframe src="https://docs.google.com/document/d/1CE0w7Pty9Vw6DGu2k9AYZlBDCehiIRRRwY8jCNduDEU/edit?usp=sharing" height="800px" width="100%" title="OpenTEAM Tech Review"></iframe>

