<h1> About the Oath of Care </h1>

<p> The Data Fiduciary Oath of Care for Agricultural Professionals is a trust-building document between individuals in advisory roles with producers. It is meant to act as a social contract that establishes good-faith activity when it comes to advising and data collection practices, as well as adherence to other OpenTEAM’s Suite of Ag Data Use Agreements. It is intended to be used by technical service and assistance providers and individuals in advisory roles with farmers, ranchers, and land stewards. </p>

<p> Relation to other documents in OpenTEAM’s Ag Data Use Suite of Documents: </p>

<ol>
  <li>References definitions in the Ag Data Use Glossary</li>
  <li>Establishes best practice for individuals in advisory roles, in order to uphold the principles set out in the Agriculturalists’ Bill of Data Rights </li>
  <li>Serve as a trust-building document that may be shared with a land steward before initiating a Proxy Agreement and accepting Conditional Terms of Use Agreements with a chosen advisor’</li>
</ol>

</br>
<h1> Versioning </h1>

<i> Version 2 Document (Released July 2023) </i>

<p>We are currently accepting feedback for Version 2.0 of the Oath of Care Starting in 2024, there will be an annual review and versioning process, with the first iterative process taking place quarter one 2024.</p>

<p>Feedback is encouraged at any time. The versioning cycle will be announced with ample time for review in OpenTEAM Working Groups (Hub & Networks, Field Methods, Tech) and in our newsletter.</p>


<h1> Provide Feedback </h1>
You may provide feedback or insights on the Oath of Care in the following ways:
<ol>
  <li>Visit and contribute to the discussion on Hylo [link] </li>
  <li>Provide comments on the linked and embedded Google doc below (to submit a comment anonymously, log out of your Google account or open the link in a private (incognito) window</li>
  <li>Contact us directly [link to contact page]</li>
</ol>

<h2> <a href="https://docs.google.com/document/d/1HVhezEh-zY0FH6J_mOCQMuJB-1xInIkEDPopeRnRB6U/edit#heading=h.izvejbdq8lh7"> Provide Comments to the Oath of Care</a> </h2>
<iframe src="https://docs.google.com/document/d/1HVhezEh-zY0FH6J_mOCQMuJB-1xInIkEDPopeRnRB6U/edit#heading=h.izvejbdq8lh7" height="800px" width="100%" title="OpenTEAM Tech Review"></iframe>
