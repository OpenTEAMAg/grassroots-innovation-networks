<h1> About the Agriculturalists’ Bill of Data Rights </h1>

<p> The Agriculturalists’ Bill of Data Rights is a  set of rights that are ensured to land stewards in regards to their data within the OpenTEAM technology ecosystem. It is the responsibility of community members within the technology ecosystem to uphold these rights on the behalf of stewards, as well as to implement mechanisms for producer control within their technologies. This document shapes all other OpenTEAM data artifacts, which are meant to maintain producers’ data rights. </p>

<p> This document is to be used by OpenTEAM’s technologist community as a roadmap to maintain producer data rights and by OpenTEAM tech ecosystem users as an educational document to understand their data rights within the ecosystem. </p>
<p>Relation to other documents in OpenTEAM’s Ag Data Use Suite of Documents:</p>
<ol>
  <li>References definitions in the Ag Data Use Glossary</li>
  <li>Establishes foundational rights which all other artifacts are meant to uphold </li>
</ol>
<h1> Versioning </h1>

<i> Version 1 Document (Released July 2023) </i>
<p>We are currently accepting feedback for Version 1.0 of the Agriculturalists’ Bill of Data Rights in 2024, there will be an annual review and versioning process, with the first iterative process taking place quarter one 2024. </p>
<p>
Feedback is encouraged at any time. The versioning cycle will be announced with ample time for review in OpenTEAM Working Groups (Hub & Networks, Field Methods, Tech) and in our newsletter. </p>


<h1> Provide Feedback </h1>
You may provide feedback or insights on the Oath of Care in the following ways:
<ol>
  <li>Visit and contribute to the discussion on Hylo [link] </li>
  <li>Provide comments on the linked and embedded Google doc below (to submit a comment anonymously, log out of your Google account or open the link in a private (incognito) window</li>
  <li>Contact us directly [link to contact page]</li>
</ol>

<h2> <a href="https://docs.google.com/document/d/1TGgQ_yFd0iJel-ksLNUfs4hZQC7aJfBGMjVX5_DGUeI/edit"> Provide Comments to the Agriculturalists’ Bill of Data Rights</a> </h2>
<iframe src="https://docs.google.com/document/d/1TGgQ_yFd0iJel-ksLNUfs4hZQC7aJfBGMjVX5_DGUeI/edit" height="800px" width="100%" title="OpenTEAM Tech Review"></iframe>
