# International in-person convening for Grassroots Technological Innovations for Agroecology 
<i> 18 to 21 of October 2023, Italy </i>

<i>Fostering collaboration between grassroots agricultural innocation networks</i>

### What is this gathering?
This convening will bring together organizations and local networks whose work uplifts smallholder
farmers’ capacity to co-create their own agricultural technologies. The convening is part of a broader
initiative to build an international Assembly of organizations cooperating to make technological

sovereignty a reality. This gathering centers a core group of organizations who are active in on-the-
ground grassroots innovation work, as a foundational step towards weaving together a larger global

Assembly that will continue to collaborate into the future.
Around 15 grassroots organizations will meet in Italy, from 18 to 21 of October 2023 (arrivals are
foreseen on 17th). The 4-days meeting will be the opportunity to explore collectively how we
develop adequate, affordable, adaptable technologies (physical and digital) with rural communities
and smallholders farmers. A detailed agenda will be suggested early September based on online
discussion. The meeting will be held in English and will allow one representative per organization
only due to budget limitations.

### Why does it matter?
This gathering takes place as industrial agriculture becomes increasingly dependent on data-use,
digitalization, and other complex technologies. We want to explore what these trends mean for us
as smallholders around the world. Are these emerging technologies suitable for our farming
activities? Are they responsive to our needs? How do they affect individual and community rights
such as our rights to privacy, land, and food?
Despite the fact that family farmers produce 80% of the world’s food, mainstream technology
continues to serve and promote bigger and bigger farms. Smallholder farmers have overcome this
lack of appropriate technologies through cooperative innovation. These grassroots technologies
allow small farms to adapt, to be financially viable, to process our own products, to save resources,
and to share knowledge. As smallholders face increasing pressure from environmental crises and
corporate consolidation, now is the time to strengthen our networks to advance technological
autonomy and sovereignty for smallholders.
### Who is the gathering for?
The gathering is for organizations and local networks who are promoting the technological
sovereignty of small-scale food producers. “Technology” encompasses physical tools, equipment,
and digital tools. We welcome organizations that representpeasants, fisherfolks, pastoralists, seed
savers, local food-processors, etc. We will collectively continue to clarify the types of organizations
needed in this international network as it evolves.
### What will we do together?
In the first global gathering of this network, we will exchange experiences with the potential to
unlock innovations that multiply and enrich our local work. This includes sharing knowledge about
critical dimensions such as organizational models, policy work, institutional arrangements, funding
strategies, digital tools, challenging the mainstream top-down culture, and alliances with
agroecology movements, social movement, academia, IT experts, and much more.
We aim to build solidarity and trust amongst organizations that are working towards our shared
goals of agroecology and food sovereignty by supporting community-led innovations. We see the
value of collective intelligence that emerges through collaboration across geographies and
organizations. Just as co-creation at the community scale can overcome local challenges,
international and regional collaboration can catalyse innovation and partnerships to address
systemic challenges. We understand that collaboration moves at the pace of trust, so the future
strategy and work of the network will be developed by the network itself as we explore the specific
opportunities for global collaboration to create meaningful change.
### Want to learn more?
See the background white paper <a href="https://docs.google.com/document/d/1lEkiNrbXQVM3xj4M7dQgHE0xSix-G0Q2XCo-TH7Yum4/edit?usp=sharing">Technological autonomy not autonomous technology</a>

See the OpenGrains video on the emergence of grassroots agricultural innovation networks