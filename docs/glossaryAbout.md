<h1> About the Ag Data Glossary </h1> 

<h1> Versioning </h1>

<i> Version 1 Document (Released July 2023) </i>

<p> We are currently accepting feedback for Version 1.0 of the glossary. Starting in 2024, there will be an annual review and versioning process, with the first iterative process taking place quarter one 2024. </p>

<p> Feedback is encouraged at any time. The versioning cycle will be announced with ample time for review in OpenTEAM Working Groups (Hub & Networks, Field Methods, Tech) and in our newsletter. </p>


<h1> Provide Feedback </h1>
You may provide feedback or insights on the Ag Data Glossary in the following ways:
<ol>
  <li>Visit and contribute to the discussion on Hylo [link] </li>
  <li>Provide comments on the linked and embedded Google doc below (to submit a comment anonymously, log out of your Google account or open the link in a private (incognito) window)</li>
  <li>Contact us directly [link to contact page]</li>
</ol>

<h2> <a href="https://docs.google.com/document/d/16nBSKVeqPQDWZ4zSeLjx-DT_cC2xt1083GSM1TBWw2w/edit?usp=sharing"> Provide Comments to the Glossary </a> </h2>
<iframe src="https://docs.google.com/document/d/16nBSKVeqPQDWZ4zSeLjx-DT_cC2xt1083GSM1TBWw2w/edit?usp=sharing" height="800px" width="100%" title="OpenTEAM Tech Review"></iframe>


