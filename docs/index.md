


<h1>Grassroots Innovations for Agroecology Assembly</h1>
<i>A global call for collaboration to strengthen small-holder food producers’ technological sovereignty</i>

<img src="assets/invite.png" alt="Grassroots Technological Innovations for Agroecology" class="imgcenter" width="200" >

### Background

Despite the fact that family farmers [produce 80% of the world’s food](https://www.fao.org/family-farming-decade/home/en/), mainstream technology continues to serve and promote bigger and bigger farms. Smallholder food producers have overcome this lack of appropriate technologies through cooperative grassroots  innovation. While this form of innovation is not a new phenomena, the difficulty of sourcing appropriate technology has spurred a growing movement of peasant-led innovation networks that are developing and sharing novel technologies and practices adapted to their agrecological farming systems. These grassroots technologies allow small producers to adapt, to be financially viable, to process their own products, to save resources, and to share knowledge. As smallholders face increasing pressure from environmental crises and corporate consolidation, now is the time to strengthen our networks to advance technological autonomy for smallholders.

Smallholder-led innovation provides a powerful and necessary alternative to the trend that has been called “AG 4.0” by the [World Government Summit](https://www.oliverwyman.com/content/dam/oliver-wyman/v2/publications/2021/apr/agriculture-4-0-the-future-of-farming-technology.pdf). From big data and precision farming to robotics and biotechnology, AG 4.0 technologies have been increasingly embraced by governments, institutions and industry as a profitable panacea to increase production efficiency, decrease labor needs, and supposedly reduce environmental damage, all while feeding the growing world population. The image of the future accompanying AG 4.0 usually includes one farmer, armed with a crew of artificially intelligent machines, managing a vast monoculture using extensive remote data collection. This vision leaves no future for small-scale food producers, farmworkers, or agroecological farmers, who often find that digital tools are not designed to meet their needs and that they cannot compete with the increasing scale of industrial farms. By aggravating issues related to data access and privacy, erasure of traditional knowledge, corporate control over farm practices, and forced migration to urban centers, AG 4.0 threatens to further centralize the food system under a new biodigital hegemony [(IPES-Food & ETC Group, 2021)](https://www.etcgroup.org/sites/www.etcgroup.org/files/files/longfoodmovementen.pdf).  

This AG 4.0 vision is already becoming a reality with increased automation of farm machinery and a surging use of corporate-backed digital tools in countries where industrial agriculture dominates. Yet the work of smallholder-led innovation networks around the world proves that alternative models of innovation already exist. Now is the time for organizations, networks, and communities to come together to articulate a shared perspective of how to describe, protect and strengthen technological sovereignty. Just as food sovereignty emphasizes the rights of communities to define and govern their food system [(Nyéléni, 2007)](https://nyeleni.org/spip.php?article290), technological sovereignty can be defined as the self-determination of food-producing communities to define the technology used in their food systems. For food sovereignty movements and smallholders to resist exploitative technologies, grassroots systems of innovation that create appropriate, adaptable and accessible technology must be uplifted.

### About this project

In response to the threats of AG 4.0 and other pressures that smallholders face, we are initiating the Assembly for Grassroots Innovations for Agroecology, a global network that collaborates to strengthen smallholders’ technological sovereignty. The Assembly is for all organizations, local networks, researchers, and individuals who share this goal, such as organizations that represent peasants, fisherfolks, pastoralists, seed savers, local food-processors, etc. This includes organizations active in the dissemination and sharing of technological innovation, where “technology” encompasses physical tools, equipment, and digital tools.

The Assembly aims to build solidarity and trust amongst organizations that are working towards our shared goals of agroecology and food sovereignty in order to mutually empower and energize each others’ work. We see the value of collective intelligence that emerges through collaboration across geographies and organizations. Just as co-creation at the community scale can overcome local challenges, international and regional collaboration can catalyse innovation and partnerships to address systemic challenges. 

We understand that collaboration moves at the pace of trust, so the future strategy of this emerging international network will be developed by the participants collectively as we explore the specific opportunities for global collaboration to create meaningful change. The work of the network will include facilitating knowledge sharing through in-person gatherings, online workshops, and conversations. We will exchange experiences in critical dimensions such as organizational models, policy work, institutional arrangements, funding strategies, digital tools, challenging the mainstream top-down culture, and alliances with agroecology movements, social movements, academia, IT experts, and much more. As a community of practice, we wish to explore what the trends of data-use, digitalization, and other complex technologies in industrial agriculture mean for us as smallholders around the world. Are these emerging technologies responsive to our needs? How do they affect individual and community rights such as our rights to privacy, land, and food? 

As a collective and global voice, this network  can strengthen advocacy efforts for the rights of smallholders’ to shape the tools and technologies that support their food systems. Crafting a shared understanding of technological sovereignty rooted in agroecology has the potential to serve as a vital counter narrative to extractive technology and innovation models. Just as the reality of AG 4.0 grew out of the capitalist imagination, our unified envisioning has the power to make technological sovereignty a reality for more food producers around the world.

### How to get involved

If your organization wishes to learn more about the Assembly, please contact us at [caroline.ledant@scholacampesina.org](mailto:caroline.ledant@scholacampesina.org)


### Want to learn more?
See the background white paper <a href="https://docs.google.com/document/d/1lEkiNrbXQVM3xj4M7dQgHE0xSix-G0Q2XCo-TH7Yum4/edit?usp=sharing">Technological autonomy not autonomous technology</a>

See the <a href="https://opengrains.cc/"> OpenGrains </a> video on the emergence of grassroots agricultural innovation networks


