- See the background white paper
<a href="https://gitlab.com/OpenTEAMAg/grassroots-innovation-networks/-/raw/master/docs/assets/Technological_autonomy.pdf" target="_blank" >Technological autonomy not autonomous technology</a>

- <a href="https://opengrains.cc/" target="_blank"> OpenGrains video on the emergence of grassroots agricultural innovation networks</a>


- <a href="https://www.csm4cfs.org/csipm-vision-statement-on-data-for-food-security-and-nutrition" target="_blank"> CSIPM Vision Statement on Data for Food security and Nutrition </a>


- <a href="https://us06web.zoom.us/rec/share/dTmOzI6Mwcjt_XKM0jHjtH7UhowIXS5rQsZTlKjQNpgmt16LPtzk5Br0gy8yEGOC.IxEAczwpRXA7an88" target="_blank"> September 28th Grassroots Innovations for Agroecology Meeting</a> 
<br> Passcode: c5k%@NYm
<br>
<i> This session included a series of short presentations of grassroots experiences of agroecology innovations and processes followed by an an open dialogue with researchers and representatives from these organizations. </i>